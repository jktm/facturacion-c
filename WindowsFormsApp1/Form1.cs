﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Sql;
using System.Configuration;
using System.IO;
using ExcelLibrary.SpreadSheet;
using Microsoft.Office.Interop.Excel;
using System.Windows;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        int cont = 0;
        float igv_total = 0;
        float igv = 0.18F;
        float total = 0;
        int jayin = 1;
        public Form1()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            cont += 1;
            string path = @"C:\Users\Javier\source\repos\lib\WindowsFormsApp1\" + DateTime.Now.ToString("dd-MM-yyyy") + @"\" + cont.ToString("D5") + ".xls";
            string subPath = @"C:\Users\Javier\source\repos\lib\WindowsFormsApp1\";
            bool exists = Directory.Exists(subPath + DateTime.Now.ToString("dd-MM-yyyy"));
            if (!exists)
                Directory.CreateDirectory(subPath + DateTime.Now.ToString("dd-MM-yyyy"));

            aExcel(grid, path);
            double parcial_venta = total / 1.18F;
            double igv_total = parcial_venta * igv;

            StreamWriter cabecera = new StreamWriter(@"D:\data0\facturador\DATA\20490241001-" + comprobante.SelectedValue.ToString() + "-" + serieCDP.SelectedValue.ToString() + "-"+jayin.ToString("D8")+".cab");
            //StreamWriter cabecera = new StreamWriter(@"C:\Users\Javier\source\repos\lib\WindowsFormsApp1\prueba\20490241001-" + comprobante.SelectedValue.ToString() + "-" + serieCDP.SelectedValue.ToString() + "-00000001.cab");
            cabecera.Write("01|" + DateTime.Now.ToString("yyyy-MM-dd") + "|0|" + documento.SelectedValue.ToString() + "|" + numDoc.Text + "|" + nombre.Text + "|PEN|0.00|0.00|0.00|" + parcial_venta.ToString("F") + "|0.00|0.00|" + igv_total.ToString("F") + "|0.00|0.00|" + total.ToString("0.00") + "|");
            cabecera.Close();

            StreamWriter detalle = new StreamWriter(@"D:\data0\facturador\DATA\20490241001-" + comprobante.SelectedValue.ToString() + "-" + serieCDP.SelectedValue.ToString() + "-"+jayin.ToString("D8")+".det");
            //StreamWriter detalle = new StreamWriter(@"C:\Users\Javier\source\repos\lib\WindowsFormsApp1\prueba\20490241001-" + comprobante.SelectedValue.ToString() + "-" + serieCDP.SelectedValue.ToString() + "-00000001.det");
            for (int i = 0; i < grid.Rows.Count; ++i)
            {
    
                string produc = "";
                int cant = 0;
                float preU = 0;
                float item, precio_igv, total_det;

                produc = grid.Rows[i].Cells[0].Value.ToString();
                cant = Convert.ToInt32(grid.Rows[i].Cells[1].Value);
                preU = float.Parse(grid.Rows[i].Cells[3].Value.ToString());

                item = cant * preU;
                precio_igv = item * igv;
                total_det = item + precio_igv;

                detalle.Write("NIU|" + cant.ToString("F") + "|||" + produc + "|" + preU.ToString("F") + "|0.00|" + precio_igv.ToString("F") + "|10|0.00|01|" + item.ToString("F") + "|" + total_det.ToString("F") + "|\r\n");
            }
            detalle.Close();
            //string nombre = "ratacanga" + cont + ".xls";
            //SaveFileDialog sfd = new SaveFileDialog();
            //sfd.Filter = "Excel Document (*.xls)|*.xls";
            //sfd.Title = "Guardar asdas";
            //sfd.FileName = "0000000"+cont+".xls";
            //myImage.Save(@"D:\Temp\myImage.jpg", System.Drawing.Imaging.ImageFormat.Jpeg);

            //if (sfd.ShowDialog()==DialogResult.OK)
            //{
            //aExcel(grid, sfd.FileName);
            //aExcel(grid,path);
            //}
            /* int cantidad = int.Parse(textBox_cant.Text);
             float precio = float.Parse(textBox_pre.Text);
             float item = cantidad * precio;
             float precio_igv = item * igv;
             float total_det = item + precio_igv;

             StreamWriter detalle = new StreamWriter(@"C:\Users\Javier\source\repos\lib\WindowsFormsApp1\prueba\20490241001-" + comprobante.SelectedValue.ToString() + "-" + serieCDP.SelectedValue.ToString() + "-00000001.det");
             detalle.Write("NIU|"+cantidad+".00||"+textBox_prod.Text+"|"+precio+"|0.00|"+precio_igv.ToString("F")+"|10|0.00|01|"+item.ToString()+"|"+total_det.ToString("F")+"|\n");
             detalle.Close();*/

            total = 0;
            grid.Rows.Clear();
            MessageBox.Show("Generado y exportado");
        }
        public void exportaraexcel(DataGridView tabla, string filename)
        {

        }
        private void button1_Click(object sender, EventArgs e)
        {
            total = 0;
            igv_total = 0;
            grid.AllowUserToAddRows = false;
            if (textBox_prod.Text != "" && textBox_cant.Text != "" && textBox_pre.Text != "" )
            {
                grid.Rows.Add(textBox_prod.Text, textBox_cant.Text, float.Parse(textBox_pre.Text), (float.Parse(textBox_pre.Text) / 1.18F).ToString("F"), (float.Parse(textBox_pre.Text) / 1.18F * int.Parse(textBox_cant.Text)).ToString("F"));
                //lista.Items.Add("Producto : " + textBox_prod.Text + " | Cantidad : " + textBox_cant.Text + " | Precio unitario : " + textBox_pre.Text + " | Precio total : " + (float.Parse(textBox_pre.Text) * int.Parse(textBox_cant.Text)));
                //total = total + (float.Parse(textBox_pre.Text) * int.Parse(textBox_cant.Text));
                /*foreach (Control c in Controls)
                    if (c.GetType() == typeof(TextBox))
                        c.Text = "";*/

                this.textBox_prod.Focus();
                this.textBox_prod.Clear();
                this.textBox_cant.Clear();
                this.textBox_pre.Clear();

                for (int i = 0; i < grid.Rows.Count; ++i)
                {
                    //total += Convert.ToInt32(grid.Rows[i].Cells[3].Value);
                    total += float.Parse(grid.Rows[i].Cells[4].Value.ToString());
                    igv_total += (float.Parse(grid.Rows[i].Cells[2].Value.ToString()) - float.Parse(grid.Rows[i].Cells[2].Value.ToString()) / 1.18F) * Convert.ToInt32(grid.Rows[i].Cells[1].Value);
                }
                total += igv_total;
                boxIGV.Text = igv_total.ToString("F");
                textBox_final.Text = total.ToString("F");

            };
  
        }

        private void aExcel(DataGridView grid, string filename)
        {
            string stOutput = "";
            string sHeaders = "";
            for (int j = 0; j < grid.Columns.Count; ++j)
                sHeaders = sHeaders.ToString() + Convert.ToString(grid.Columns[j].HeaderText) + "\t";
            stOutput += sHeaders + "\r\n";
            for (int k = 0; k < grid.RowCount; ++k)
            {
                string stline = "";
                for (int l = 0; l < grid.Rows[k].Cells.Count; ++l)
                    stline = stline.ToString() + Convert.ToString(grid.Rows[k].Cells[l].Value) + "\t";
                stOutput += stline + "\r\n";
            }
            Encoding utf16 = Encoding.GetEncoding(1254);
            byte[] output = utf16.GetBytes(stOutput);
            FileStream fs = new FileStream(filename, FileMode.Create);
            BinaryWriter bw = new BinaryWriter(fs);
            bw.Write(output, 0, output.Length);
            bw.Flush();
            bw.Close();
            fs.Close();
            //grid.Rows.Clear();
            textBox_final.Clear();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            /*op.Items.Add("01-Venta interna");

            comprobante.Items.Add("01-Factura");
            comprobante.Items.Add("03-Boleta");
            comprobante.Items.Add("07-Nota de Crédito");
            comprobante.Items.Add("08-Nota de Débito");
            comprobante.Items.Add("09-Guía de Remisión de remitente");

            
            serieCDP.Items.Add("B001");
            serieCDP.Items.Add("F001");

            documento.Items.Add("0-No Doc");
            documento.Items.Add("1-DNI");
            documento.Items.Add("4-Carne de Extranjeria");
            documento.Items.Add("6-RUC");
            documento.Items.Add("7-Pasaporte");
            documento.Items.Add("A-Ced. Diplomatica de identidad");*/

            op.DisplayMember = "Text";
            op.ValueMember = "Value";

            comprobante.DisplayMember = "Text";
            comprobante.ValueMember = "Value";

            serieCDP.DisplayMember = "Text";
            serieCDP.ValueMember = "Value";

            documento.DisplayMember = "Text";
            documento.ValueMember = "Value";

            var itemsOP = new[] {
                new { Text = "01-Venta interna", Value = "01" }
            };

            var itemsComp = new[] {
                new { Text = "Boleta", Value = "03" },
                new { Text = "Factura", Value = "01" },
                new { Text = "Nota de Crédito", Value = "07" },
                new { Text = "Nota de Débito", Value = "08" },
                new { Text = "Guía de Remisión de remitente", Value = "09" }
            };

            var itemsSerie = new[] {
                new { Text = "B001", Value = "B001" },
                new { Text = "F001", Value = "F001" },
            };

            var itemsDoc = new[] {
                new { Text = "DNI", Value = "1" },
                new { Text = "RUC", Value = "6" },
                new { Text = "No Doc", Value = "0" },
                new { Text = "Carne de Extranjeria", Value = "4" },
                new { Text = "Pasaporte", Value = "7" },
                new { Text = "Ced. Diplomatica de identidad", Value = "A" },
            };

            op.DataSource = itemsOP;
            comprobante.DataSource = itemsComp;
            serieCDP.DataSource = itemsSerie;
            documento.DataSource = itemsDoc;
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            //valor2.Text = comprobante.SelectedValue.ToString();
        }

        private void comboBox3_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void comboBox2_SelectedIndexChanged_1(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void label12_Click(object sender, EventArgs e)
        {

        }

        private void serieCDP_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void textBox5_TextChanged(object sender, EventArgs e)
        {

        }

        private void comboBox3_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            valor.Text = documento.SelectedValue.ToString();
        }

        private void label12_Click_1(object sender, EventArgs e)
        {

        }

        private void textBox6_TextChanged(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {

            //this.lista.Items.RemoveAt(this.lista.SelectedIndex);
            grid.Rows.RemoveAt(grid.CurrentCell.RowIndex);
            //total de venta
            for (int i = 0; i < grid.Rows.Count; ++i)
            {
                //total += Convert.ToInt32(grid.Rows[i].Cells[3].Value);
                total += float.Parse(grid.Rows[i].Cells[4].Value.ToString());
                igv_total += (float.Parse(grid.Rows[i].Cells[2].Value.ToString()) - float.Parse(grid.Rows[i].Cells[2].Value.ToString()) * igv) * Convert.ToInt32(grid.Rows[i].Cells[1].Value);
            }
            total += igv_total;
            textBox_final.Text = total.ToString("F");
            boxIGV.Text = igv_total.ToString("F");
            igv_total = 0;
            total = 0;
            textBox_prod.Focus();
            textBox_final.Clear();

        }

        private void button4_Click(object sender, EventArgs e)
        {

        }

        private void textBox_final_TextChanged(object sender, EventArgs e)
        {

        } 

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }

        private void grid_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            /*var item = grid.Rows[e.RowIndex].Cells[1].Value;
            MessageBox.Show(item.ToString());*/
        }

        private void valor_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged_1(object sender, EventArgs e)
        {

        }
    }
}
